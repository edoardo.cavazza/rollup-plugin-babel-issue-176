import babel from 'rollup-plugin-babel';
import common from 'rollup-plugin-commonjs';

export default {
    input: 'src/index.js',
    output: {
        file: 'dist/bundle.js',
        format: 'umd',
    },
    plugins: [
        babel(
            // { exclude: ['src/umd.js'] }
        ),
        common(),
    ],
};