Repository sample for [https://github.com/rollup/rollup-plugin-babel/issues/176](https://github.com/rollup/rollup-plugin-babel/issues/176).

## Build

```
$ [npm/yarn] install
$  npm start
```

## Options:

Uncomment line 12 in `rollup.config.js` in order to bundle without transpiling umd source.